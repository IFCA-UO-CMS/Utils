#!/bin/bash
###########################################################
# List the variables in the analysis code for a MiniTree
#
# SYNTAX: findvarsfromanalysis.sh MyAnalsyisMiniTrees.C
###########################################################

help() {
cat <<EOF
SYNTAX: $0 [-h] MyAnalsyisMiniTrees.C [outputfile]

Lists the variables in the analysis code for a MiniTree

OPTIONS:
  -h: Prints this help

  MyAnalsyisMiniTrees.C: PAF Analysis file

  outputfile: File were the list of variables will be saved. 
              Defaults to inputfile.log
  
EOF
}

if [ -z $1 ]; then
    echo "ERROR: I need the MyAnalsyisMiniTrees.C file as an argument"
    help
    exit
fi

ifile=$1

if [ "$1" == "-h" ]; then
    help
    exit
fi

if [ -z $2 ]; then
    outputfile=`basename $ifile|cut -d. -f 1`.log
else
    outputfile=$2
fi

#Some variables
rawvars=/tmp/variablesraw.log

#Find the lines with at least a variable starting with T_
grep T_ $ifile > $rawvars

#Move to a new line the variable provided it is not something like HLT_...
cat $rawvars|sed 's/(T_/\nT_/g'  > $rawvars.2
cat $rawvars.2|sed 's/)T_/\nT_/g'  > $rawvars.3
cat $rawvars.3|sed 's/ T_/\nT_/g'  > $rawvars.4
cat $rawvars.4|sed 's/,T_/\nT_/g'  > $rawvars.5
cat $rawvars.5|sed 's/<T_/\nT_/g'  > $rawvars.6
cat $rawvars.6|sed 's/+T_/\nT_/g'  > $rawvars.7
cat $rawvars.7|sed 's/-T_/\nT_/g'  > $rawvars.8
cat $rawvars.8|sed 's/*T_/\nT_/g'  > $rawvars.9
cat $rawvars.9|sed 's|/T_|\nT_|g'  > $rawvars.10
cat $rawvars.10|sed 's/\tT_/\nT_/g'  > $rawvars.11
cat $rawvars.11|sed 's/!T_/\nT_/g'  > $rawvars.20

#Get rid of the lines with no variables
grep T_  $rawvars.20> $rawvars.21
grep -v \#if $rawvars.21> $rawvars.30

#Cut the text to leave only the lines with a variable
cat $rawvars.30 | cut -d\  -f 1 > $rawvars.31
cat $rawvars.31 | cut -d\)  -f 1 > $rawvars.32
cat $rawvars.32 | cut -d\-  -f 1 > $rawvars.33
cat $rawvars.33 | cut -d\;  -f 1 > $rawvars.34
cat $rawvars.34 | cut -d\*  -f 1 > $rawvars.35
cat $rawvars.35 | cut -d\.  -f 1 > $rawvars.36
cat $rawvars.36 | cut -d\,  -f 1 > $rawvars.37
cat $rawvars.37 | cut -d\<  -f 1 > $rawvars.38
cat $rawvars.38 | cut -d\>  -f 1 > $rawvars.39
cat $rawvars.39 | cut -d\=  -f 1 > $rawvars.40

#Remove obsolete variables
grep -v T_pfElec_ConvInfoDCot $rawvars.40| grep -v T_pfElec_ConvInfoDist | grep -v T_pfElec_nLost| grep -v T_TriggerParticle_ > $rawvars.50

#Remove HLT variables since they oscilate
grep -v T_HLT $rawvars.50 > $rawvars.60

#Remove empty lines, sort and get remove repeated vars
echo "Saving final output to $outputfile..."
grep -v '^$' $rawvars.60 | sort | uniq > $outputfile


rm -f ${rawvars}.*
