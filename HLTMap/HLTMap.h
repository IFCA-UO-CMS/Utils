///////////////////////////////////////////////////////////////////////
//
//    FILE: HLTMap.h
//   CLASS: HLTMap
// AUTHORS: Santiago Folgueras
//    DATE: 23/03/2011
//
// CONTENT: An utility class to generate a map for the HLT trigger bits
//          for each run, and being able to read it from the TESCONTuples
//            
//
///////////////////////////////////////////////////////////////////////

#ifndef HTLMAP_H
#define HLTMAP_H 1

#include "TNamed.h"
#include <map>
#include "TTree.h"
#include "TString.h"

class HLTMap {
 public:
  HLTMap(std::vector<TString> filenames);
  HLTMap(TTree* RunInfo = 0);
  ~HLTMap() {delete fTree;}
  
  // Tells if this object has loaded correctly the histograms
  bool IsValid() const {return fRunNumber;}
  
  void GetHLTNames(Int_t run);
  int  GetHLTPrescale(const char* hltname, int *HLTPrescale, Int_t  RunNumber);
  int  GetHLTBit(std::string theHltName);
  int  GetRunNumber() const {return fRunNumber;}
  bool GetHLTResult(const char* hltname, int *HLTResults, Int_t RunNumber);

  void SetHLTTree(TTree* RunInfo);
    
  void Dump();

 protected:
  int fRunNumber;
  TTree *fTree;
  std::vector<std::string> *fHLTNames;
  std::map<std::string,int> fHLTLabelMap;
};

#endif
