///////////////////////////////////////////////////////////////////////
//
//    FILE: ElectronSelectionTESCO.h
//   CLASS: ElectronSelectionTESCO
// AUTHORS: S. Folgueras
//    DATE: 27/06/2011
//
// CONTENT: This class for PAF standarizes the selection of good electrons
//          for analysis
//
///////////////////////////////////////////////////////////////////////

#ifndef ELECTRONSELECTION_H
#define ELECTRONSELECTION_H 1

// PAF includes and forward declarations
#include "packages/CMSAnalysisSelectorTESCO/CMSAnalysisSelectorTESCO.h"
#include "packages/MuonSelectionTESCO/MuonSelectionTESCO.h"

// STL includes
#include <vector>
#include <iostream>

class ElectronSelectionTESCO {
 public:

  //Constructors and destructor
  //---------------------------
  ElectronSelectionTESCO(CMSAnalysisSelectorTESCO* selector,MuonSelectionTESCO* muon);
  virtual ~ElectronSelectionTESCO();

  //Get Methods
  //-----------
  std::vector<unsigned int>* GetBasicElectrons();
  std::vector<unsigned int>* GetTightElectrons();
  std::vector<unsigned int>* GetLooseElectrons();
  
  unsigned int GetNBasicElectrons()   {return GetBasicElectrons()->size();   }
  unsigned int GetNTightElectrons()   {return GetTightElectrons()->size();   }
  unsigned int GetNLooseElectrons()   {return GetLooseElectrons()->size();   }
  
  Int_t GetCurrentEvent() const {return fIEvent;}

  //Variables used for cuts from Input Parameters
  //---------------------------------------------
  //   - Pt / Eta / DeltaPt/Pt
  void SetMinElPt(float cutMinElPt)                        {fCutMinElPt=cutMinElPt;}
  void SetMaxElEta(float cutMaxElEta)                      {fCutMaxElEta=cutMaxElEta;}
  //   - IP and DeltaZ of track associated with muon w.r.t PV
  void SetMaxElD0PV(float cutMaxElD0PV)                    {fCutMaxElD0PV=cutMaxElD0PV;}
  void SetMaxElDzPV(float cutMaxElDzPV)                    {fCutMaxElDzPV=cutMaxElDzPV;}
  
  //Activates the selection mechanism
  //------------------
  void SelectElectrons();
  
  //Dump cut values
  //---------------
  void DumpCutValues(std::ostream& os = std::cout) const;
  void DumpElectronValues(std::ostream& os = std::cout) const;
  
  //Other helper methods
  //--------------------
  bool IsGoodBasicElectron(int iElectron) const;
  bool IsGoodElecID_WP90(int iElectron) const;
  bool IsGoodElecID_WP80(int iElectron) const;
  bool IsTightElectron(int iElectron) const;
  bool IsLooseElectron(int iElectron) const;
  float RelElecIso(int iElec) const;
  
  bool IsGoodPrimElectron(unsigned int iElec, float ptcut) const;
  bool IsGoodSecElectron(unsigned int iElec, float ptcut) const;
  bool IsFakeElectron(unsigned int iElec) const;
  bool IsPromptElectron(unsigned int iElec) const;
  bool IsChargeMatchedElectron(unsigned int iElec) const;

 protected:
  //Protected selection methods
  //-----------------
  unsigned int SelectBasicElectrons();
  unsigned int SelectTightElectrons();
  unsigned int SelectLooseElectrons();

  bool InNewEvent() const {return (fSelector->Event!=fIEvent);}
  int GetType(int particleID) const;

 protected:
  // The CMS Analysis Selector for Minitrees
  //----------------------------------------
  CMSAnalysisSelectorTESCO* fSelector;

  // The last event processed
  Int_t fIEvent;

  // Muon Selection class to clean electrons
  MuonSelectionTESCO* fMuonSelection;
  std::vector<unsigned int> *fSelTightMuons;

  // Variables used to hold indices to the leptons
  //----------------------------------------------
  std::vector<unsigned int> *fSelBasicElectrons;   //Electrons passing basic cuts
  std::vector<unsigned int> *fSelTightElectrons;   //Electrons passing tight cuts
  std::vector<unsigned int> *fSelLooseElectrons;   //Electrons passing loose cuts

  // Variables used for cuts from Input Parameters
  //----------------------------------------------------------------------------
  float fCutMinElPt;
  float fCutMaxElEta;
  float fCutMaxElD0PV;
  float fCutMaxElDzPV;
  ClassDef(ElectronSelectionTESCO,0);

};

#endif
