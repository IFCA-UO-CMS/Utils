///////////////////////////////////////////////////////////////////////
//
//    FILE: JetSelectionTESCO.h
//   CLASS: JetSelectionTESCO
// AUTHORS: S. Folgueras
//    DATE: 27/06/2011
//
// CONTENT: This class for PAF standarizes the selection of good jets
//          for analysis
//
///////////////////////////////////////////////////////////////////////
#ifndef JETSELECTION_H
#define JETSELECTION_H 1

// PAF includes and forward declarations
#include "packages/CMSAnalysisSelectorTESCO/CMSAnalysisSelectorTESCO.h"
#include "packages/MuonSelectionTESCO/MuonSelectionTESCO.h"
#include "packages/ElectronSelectionTESCO/ElectronSelectionTESCO.h"

// STL includes
#include <vector>
#include <iostream>

class JetSelectionTESCO {
 public:

  //Constructors and destructor
  JetSelectionTESCO(CMSAnalysisSelectorTESCO* selector,MuonSelectionTESCO* muon, ElectronSelectionTESCO* electron);
  virtual ~JetSelectionTESCO();

  //Get Methods
  std::vector<unsigned int>* GetBasicPFJets();
 
  unsigned int GetNBasicPFJets()     {return GetBasicPFJets()->size();     }

  Int_t GetCurrentEvent() const {return fIEvent;}

  //Variables used for cuts from Input Parameters
  void SetMinJetPt(float cutMinJetPt)    {fCutMinJetPt=cutMinJetPt;}
  void SetMaxJetEta(float cutMaxJetEta)  {fCutMaxJetEta=cutMaxJetEta;}
  
  //Dump cut values
  void DumpJetValues(std::ostream& os = std::cout) const;
  
  // Other helper methods
  bool IsGoodBasicPFJet(int iJet) const;

 protected:
  //Protected selection methods
  void SelectJets();

  unsigned int SelectBasicPFJets();

  //Other helper methods
  bool InNewEvent() const {return (fSelector->Event!=fIEvent);}

 protected:
  // The CMS Analysis Selector for Minitrees
  CMSAnalysisSelectorTESCO* fSelector;

  // The last event processed
  Int_t fIEvent;

  // Variables used to hold indices of the jets
  std::vector<unsigned int> *fSelBasicPFJets;   //Jets passing basic cuts

  // Muon and Electron Selection Classes for Jet cleaning
  MuonSelectionTESCO* fMuonSelection;
  ElectronSelectionTESCO* fElectronSelection;
  
  std::vector<unsigned int> *fSelTightMuons;
  std::vector<unsigned int> *fSelTightElectrons;
  
  // Variables used for cuts from Input Parameters
  float fCutMinJetPt;
  float fCutMaxJetEta;
  
  ClassDef(JetSelectionTESCO,0);
};

#endif
