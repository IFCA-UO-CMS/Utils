#!/bin/bash
###########################################################
# List the variables in the analysis code for a MiniTree
#
# SYNTAX: findvarsfromselector.sh CMSAnalysisSelectorXXX.h [outputfile]
###########################################################
help() {
cat <<EOF

SYNTAX: findvarsfromselector.sh [-h] CMSAnalysisSelectorXXX.h [outputfile]

OPTIONS:
  -h: Prints this help

  CMSAnalysisSelectorXXX.h: TESCO or MiniTrees file produced with PAF. Usually
  under the packages/CMSAnalysisSelectorXXX directory

  outputfile: File were the list of variables will be saved. Defaults to var.log
  
EOF
}


if [ -z $1 ]; then
    echo "ERROR: I need the CMSAnalsyisSelectorXXX.h file as an argument"
    help
    exit
fi

ifile=$1

if [ "$1" == "-h" ]; then
    help
    exit
fi

if [ -z $2 ]; then
    ofile=vars.log
else
    ofile=$2
fi

echo "Parsing $ifile..."
grep "*b_" $ifile | sed 's/*b_/\n/g' | grep -v TBranch | sed 's/;/\n;/g' | grep -v \; | sort | uniq > $ofile
echo "Output saved in $ofile"