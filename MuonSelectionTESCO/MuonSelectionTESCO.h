///////////////////////////////////////////////////////////////////////
//
//    FILE: MuonSelectionTESCO.h
//   CLASS: MuonSelectionTESCO
// AUTHORS: S. Folgueras , I. Gonzalez Caballero
//    DATE: 27/06/2011
//
// CONTENT: This class for PAF standarizes the selection of good muons 
//          for analysis
//
///////////////////////////////////////////////////////////////////////
#ifndef MUONSELECTION_H
#define MUONSELECTION_H 1

// PAF includes and forward declarations
#include "packages/CMSAnalysisSelectorTESCO/CMSAnalysisSelectorTESCO.h"

// STL includes
#include <vector>
#include <iostream>

class MuonSelectionTESCO {
 public:

  //Constructors and destructor
  MuonSelectionTESCO(CMSAnalysisSelectorTESCO* selector);
  virtual ~MuonSelectionTESCO();
  
  //Get Methods
  std::vector<unsigned int>* GetCustomMuons();
  std::vector<unsigned int>* GetBasicMuons();
  std::vector<unsigned int>* GetTightMuons();
  std::vector<unsigned int>* GetLooseMuons();
  
  unsigned int GetNCustomMuons()   {return GetCustomMuons()->size();   }
  unsigned int GetNBasicMuons()   {return GetBasicMuons()->size();   }
  unsigned int GetNTightMuons()   {return GetTightMuons()->size();   }
  unsigned int GetNLooseMuons()   {return GetLooseMuons()->size();   }
  
  Int_t GetCurrentEvent() const {return fIEvent;}

  //Variables used for cuts from Input Parameters
  void SetMinMuPt(float cutMinMuPt)                        {fCutMinMuPt=cutMinMuPt;}
  void SetMaxMuEta(float cutMaxMuEta)                      {fCutMaxMuEta=cutMaxMuEta;}
  void SetMaxMuPtEOverMuPt(float cutMaxDeltaMuPtEOverMuPt) {fCutMaxMuPtEOverMuPt=cutMaxDeltaMuPtEOverMuPt;}
  //   - IP and DeltaZ of track associated with muon w.r.t PV
  void SetMaxMuD0PV(float cutMaxMuD0PV)                    {fCutMaxMuD0PV=cutMaxMuD0PV;}
  void SetMaxMuDzPV(float cutMaxMuDzPV)                    {fCutMaxMuDzPV=cutMaxMuDzPV;}
  //   - Chi2 of global track associated to muon
  void SetMaxMuNChi2(float cutMaxMuNChi2)                  {fCutMaxMuNChi2=cutMaxMuNChi2;}
  //   - N hits of pixel, inner and SA track associated to muon
  void SetMinMuNTkHits(int cutMinMuNTkHits)                {fCutMinMuNTkHits=cutMinMuNTkHits;}
  void SetMinMuNMuHits(int cutMinMuNMuHits)                {fCutMinMuNMuHits=cutMinMuNMuHits;}
  //   - Isolation: (PTtraks + ETcalo)/PTmuon
  void SetMaxMuRelIso03(float cutMaxMuRelIso03)            {fCutMaxMuRelIso03=cutMaxMuRelIso03;}
  void SetMaxMuIso03EMVetoEt(float cutMaxMuIso03EMVetoEt)  {fCutMaxMuIso03EMVetoEt=cutMaxMuIso03EMVetoEt;}
  void SetMaxMuIso03HadVetoEt(float cutMaxMuIso03HadVetoEt){fCutMaxMuIso03HadVetoEt=cutMaxMuIso03HadVetoEt;}
  
  //Dump cut values
  void DumpCutValues(std::ostream& os = std::cout) const;
  void DumpMuonValues(std::ostream& os = std::cout) const;

  bool IsGoodBasicMuon(unsigned int iMuon) const;
  bool IsTightMuon(unsigned int iMuon) const;
  bool IsLooseMuon(unsigned int iMuon) const;
  
  bool IsFakeMuon(unsigned int iMuon) const;
  bool IsPromptMuon(unsigned int iMuon) const;
  bool IsChargeMatchedMuon(unsigned int iMuon) const;
  bool IsGoodPrimMuon(unsigned int iMuon, float ptcut) const;
  bool IsGoodSecMuon(unsigned int iMuon, float ptcut) const;

 protected:
  //Protected selection methods
  //-----------------
  void SelectMuons();
  unsigned int SelectCustomMuons();
  unsigned int SelectBasicMuons();
  unsigned int SelectTightMuons();
  unsigned int SelectLooseMuons();

  //Other helper methods
  //--------------------
  bool InNewEvent() const {return (fSelector->Event!=fIEvent);}
  int GetType(int particleID) const;

 protected:
  // The CMS Analysis Selector for Minitrees
  CMSAnalysisSelectorTESCO* fSelector;

  // The last event processed
  Int_t fIEvent;

  // Variables used to hold indices to the leptons
  std::vector<unsigned int> *fSelCustomMuons;  //Muons passing custom cuts;
  std::vector<unsigned int> *fSelBasicMuons;   //Muons passing basic cuts
  std::vector<unsigned int> *fSelTightMuons;   //Muons passing tight cuts
  std::vector<unsigned int> *fSelLooseMuons;   //Muons passing loose cuts

  // Variables used for cuts from Input Parameters
  float fCutMinMuPt;
  float fCutMaxMuEta;
  float fCutMaxMuPtEOverMuPt;
  float fCutMaxMuD0PV;
  float fCutMaxMuDzPV;
  float fCutMaxMuNChi2;
  int fCutMinMuNTkHits;
  int fCutMinMuNMuHits;
  float fCutMaxMuIso03EMVetoEt;
  float fCutMaxMuIso03HadVetoEt;
  float fCutMaxMuRelIso03;

  ClassDef(MuonSelectionTESCO,0);

};

#endif
